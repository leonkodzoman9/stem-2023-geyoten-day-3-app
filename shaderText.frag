#version 460 core

layout (location = 0) out vec4 finalColor;

layout (binding = 0) uniform sampler2DArray textureSampler;

uniform vec4 colorFill;
uniform vec4 colorOutline;

uniform float min1;
uniform float min2;
uniform float max1;
uniform float max2;

in vec2 fragmentUV;
in flat int layer;



void main() {   

    float sd = texture(textureSampler, vec3(fragmentUV, layer)).r;
    
    if (sd < min1) { discard; }

    vec4 color = vec4(0);
    if (sd >= min1 && sd < max1) {
        color = mix(vec4(colorOutline.xyz, 0), colorOutline, smoothstep(min1, max1, sd));
    }
    else if (sd >= max1 && sd < min2) {  
        color = colorOutline;
    }
    else if (sd >= min2 && sd < max2) {
        color = mix(colorOutline, colorFill, smoothstep(min2, max2, sd));
    }
    else if (sd >= max2) {
        color = colorFill;
    }

    finalColor = color;
}