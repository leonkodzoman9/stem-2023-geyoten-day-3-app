#pragma once

#include "Includes.hpp"

#include "ShaderGL.hpp"
#include "BufferGL.hpp"
#include "BufferDescriptorGL.hpp"
#include "Texture2DGL.hpp"
#include "Mesh.hpp"



using MeshID = uint32_t;



struct Transform;
struct CameraComponent;



struct MeshData {

	BufferGL vertices;
	BufferGL indices;
	BufferDescriptorGL descriptor;

	std::shared_ptr<Texture2DGL> texture;
	glm::vec4 color;

	Mesh mesh;
};



class MeshRenderer {

private:

	ShaderGL shaderMesh;

	std::unordered_map<MeshID, MeshData> meshData;

	UniformLocation locationColor;
	UniformLocation locationHaveTexture;

public:

	MeshRenderer();

	void setOrthographic(glm::mat4 orthographic);

	void updateMesh(MeshID meshID, Mesh mesh);
	void removeMesh(MeshID meshID);

	void render();
};



