#version 460 core

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec2 vertexUV;

uniform mat4 orthographic;



out vec4 fragmentColor;
out vec2 fragmentUV;



void main() {

    gl_Position = orthographic * vec4(vertexPosition, 1);

    fragmentUV = vertexUV;
} 
