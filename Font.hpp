#pragma once

#include "Includes.hpp"

#include "Image.hpp"
#include "Texture2DArrayGL.hpp"



struct Symbol {
    int layer = 0;
    glm::vec2 uv = glm::vec2(0);
        
    glm::vec2 bearing = glm::vec2(0);
    glm::vec2 size = glm::vec2(0);
    int advance = 0;
};



class Font {

private:

    bool threadRunning;
    std::thread loaderThread;
    std::mutex mutex;
    std::condition_variable condition;

    std::unordered_map<char32_t, bool> symbolsToLoad;
    std::unordered_map<char32_t, std::pair<Image<uint8_t, 1>, Symbol>> symbolsLoaded;

    std::string name;
    Texture2DArrayGL fontAtlas;
    int layersAllocated = 0;
    int layersUsed = 0;
    glm::ivec2 atlasSize = glm::ivec2(0);

    int resolution = 0;
    std::unordered_map<char32_t, Symbol> symbols;

public:

    Font() = default;
    Font(std::string_view fontName, int resolution = 64);
    ~Font();

    Font(const Font& rhs) = delete;
    Font& operator=(const Font& rhs) = delete;
    Font(Font&& rhs) = delete;
    Font& operator=(Font&& rhs) = default;

    const Texture2DArrayGL& getAtlas() const;
    glm::ivec2 getAtlasSize() const;

    int getResolution() const;
    bool isLoaded(char32_t character);
    Symbol& getSymbol(char32_t character);

private:

    void loaderFunction();

};



