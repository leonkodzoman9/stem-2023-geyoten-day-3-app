#include "Font.hpp"

#include "ImageUtility.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H  



Font::Font(std::string_view fontName, int resolution) {

	this->resolution = resolution;
	this->name = fontName;

	this->threadRunning = true;
	this->loaderThread = std::thread([&]() { return this->loaderFunction(); });
}
Font::~Font() {

	{
		std::lock_guard(this->mutex);
		this->threadRunning = false;
		this->condition.notify_all();
	}
	this->loaderThread.join();
}

const Texture2DArrayGL& Font::getAtlas() const {

	return this->fontAtlas;
}
glm::ivec2 Font::getAtlasSize() const {

	return this->atlasSize;
}

int Font::getResolution() const {

	return this->resolution;
}
bool Font::isLoaded(char32_t character) {

	bool loaded = this->symbols.find(character) != this->symbols.end();

	{
		std::lock_guard guard(this->mutex);

		if (
			!loaded &&
			this->symbolsToLoad.find(character) == this->symbolsToLoad.end() &&
			this->symbolsLoaded.find(character) == this->symbolsLoaded.end())
		{
			this->symbolsToLoad[character] = false;
			this->condition.notify_one();
		}

		if (this->symbolsLoaded.size() > 0) {

			auto& [character, imageSymbolPair] = *this->symbolsLoaded.begin();
			auto& [image, symbol] = imageSymbolPair;

			symbol.layer = this->symbols.size();
			symbol.uv = image.size;
			this->symbols[character] = symbol;

			glm::ivec2 maxSize = this->atlasSize;
			maxSize.x = std::max(maxSize.x, image.size.x);
			maxSize.y = std::max(maxSize.y, image.size.y);

			Texture2DArrayGL newAtlas;
			newAtlas.allocateStorage(glm::ivec3(maxSize.x, maxSize.y, this->symbols.size()), TexturePixelFormat::R8, 1);
			newAtlas.setParameter(TextureParameterName::MIN_FILTER, TextureParameterValue::LINEAR);
			newAtlas.setParameter(TextureParameterName::MAG_FILTER, TextureParameterValue::LINEAR);
			newAtlas.setParameter(TextureParameterName::WRAP_S, TextureParameterValue::CLAMP_TO_EDGE);
			newAtlas.setParameter(TextureParameterName::WRAP_T, TextureParameterValue::CLAMP_TO_EDGE);

			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

			if (this->atlasSize != glm::ivec2(0)) {
				newAtlas.copyFrom(this->fontAtlas, glm::ivec3(0), glm::ivec3(0), glm::ivec3(this->atlasSize, this->symbols.size() - 1), 0);
			}
			newAtlas.updateData(image.data.data(), glm::ivec3(0, 0, this->symbols.size() - 1), glm::ivec3(image.size, 1), DataFormat::RED, DataType::UNSIGNED_BYTE, 0);

			glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

			this->fontAtlas = std::move(newAtlas);
			this->atlasSize = maxSize;

			this->symbolsLoaded.erase(character);
		}
	}
		
	return loaded;
}
Symbol& Font::getSymbol(char32_t character) {

	return this->symbols[character];
}

void Font::loaderFunction() {

	FT_Library library;
	if (FT_Init_FreeType(&library)) {
		printf("Could not initialize FreeType.\n");
		throw;
	}

	FT_Face face;
	if (FT_New_Face(library, ("C:/Windows/Fonts/" + std::string(this->name) + ".ttf").data(), 0, &face)) {
		printf("Could not load font.\n");
		throw;
	}

	while (true) {

		char32_t characterToLoad = 0;
		{
			std::unique_lock lock(this->mutex);
			this->condition.wait(lock, [&]() { return !this->threadRunning || this->symbolsToLoad.size() > 0; });

			if (!this->threadRunning) { break; }

			for (auto& [character, isBeingLoaded] : this->symbolsToLoad) {
				if (!isBeingLoaded) {
					characterToLoad = character;
					isBeingLoaded = false;
					break;
				}
			}
		}

		if (characterToLoad == 0) { continue; }

		FT_UInt glyph_index = FT_Get_Char_Index(face, characterToLoad);

		FT_Set_Pixel_Sizes(face, 0, this->resolution * 4);
		if (FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER)) {
			continue;
		}
		glm::ivec2 upscaledSize = glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);

		Image<uint8_t, 1> glyph(upscaledSize);
		std::memcpy(glyph.data.data(), face->glyph->bitmap.buffer, upscaledSize.x * upscaledSize.y);

		FT_Set_Pixel_Sizes(face, 0, this->resolution);
		if (FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER)) {
			continue;
		}
		glm::ivec2 actualSize = glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);

		Image<uint8_t, 1> image;
		if (actualSize.x > 0) {

			int spread = this->resolution / 2;
			Image<uint8_t, 1> glyphPreSDF(upscaledSize + spread * 2);
			for (int i = 0; i < upscaledSize.y; i++) {
				for (int j = 0; j < upscaledSize.x; j++) {
					glyphPreSDF.at(i + spread, j + spread) = glyph.at(i, j);
				}
			}

			image = ImageUtility::resize(ImageUtility::generateSDF(glyphPreSDF, spread), actualSize, SamplingMode::LINEAR);
		}
		else {
			image = Image<uint8_t, 1>(glm::ivec2(1));
		}

		Symbol symbol;
		symbol.bearing = glm::vec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
		symbol.advance = face->glyph->advance.x / 64;
		symbol.size = actualSize;

		std::lock_guard guard(this->mutex);

		this->symbolsLoaded[characterToLoad].first = image;
		this->symbolsLoaded[characterToLoad].second = symbol;
		this->symbolsToLoad.erase(characterToLoad);
	}

	FT_Done_Face(face);
	FT_Done_FreeType(library);
}

