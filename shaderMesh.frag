#version 460 core

layout (location = 0) out vec4 outputColor;



layout (binding = 0) uniform sampler2D tex;



uniform vec4 color;
uniform int haveTexture;



in vec2 fragmentUV;



void main() {

    vec4 texColor = haveTexture == 1 ? texture(tex, fragmentUV) : vec4(1);
    
    vec4 finalColor = texColor * color;

    if (finalColor.a == 0) { discard; }

    outputColor = finalColor;
}


