#include "MeshRenderer.hpp"

#include "Mesh.hpp"

#include "BufferGL.hpp"
#include "BufferDescriptorGL.hpp"
#include "ShaderGL.hpp"
#include "Texture2DGL.hpp"
#include "Window.hpp"



MeshRenderer::MeshRenderer() {

	this->shaderMesh.addShaderStage("shaderMesh", ShaderStage::VERTEX);
	this->shaderMesh.addShaderStage("shaderMesh", ShaderStage::FRAGMENT);
	this->shaderMesh.createProgram();

	this->locationColor = this->shaderMesh.getUniformLocation("color");
	this->locationHaveTexture = this->shaderMesh.getUniformLocation("haveTexture");
}

void MeshRenderer::setOrthographic(glm::mat4 orthographic) {

	this->shaderMesh.use();
	this->shaderMesh.setMatrix4f("orthographic", orthographic);
}

void MeshRenderer::render() {

	for (auto& [meshID, meshData] : this->meshData) {

		if (!meshData.mesh.enabled) { continue; }

		this->shaderMesh.use();
		this->shaderMesh.setVector4f("color", meshData.color);
		this->shaderMesh.setInt("haveTexture", meshData.texture != nullptr);

		if (meshData.texture != nullptr) {
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, meshData.texture->getID());
		}

		if (meshData.mesh.indices.size() > 0) {
			meshData.descriptor.use();
			glDrawElements(GL_TRIANGLES, meshData.mesh.indices.size(), GL_UNSIGNED_INT, nullptr);
		}
	}
}

void MeshRenderer::updateMesh(MeshID meshID, Mesh mesh) {
		
	MeshData& meshData = this->meshData[meshID];

	meshData.color = mesh.color;
	meshData.texture = mesh.texture;

	bool changedBuffers = false;
	if (meshData.mesh.vertices.size() != mesh.vertices.size()) {
		meshData.mesh.vertices = mesh.vertices;
		meshData.vertices = BufferGL();
		meshData.vertices.allocateStorage(mesh.vertices.data(), (int)mesh.vertices.size() * sizeof(Vertex));
		changedBuffers = true;
	}
	else {
		meshData.vertices.updateData(mesh.vertices.data(), (int)mesh.vertices.size() * sizeof(Vertex), 0);
	}

	if (meshData.mesh.indices.size() != mesh.indices.size()) {
		meshData.mesh.indices = mesh.indices;
		meshData.indices = BufferGL();
		meshData.indices.allocateStorage(mesh.indices.data(), (int)mesh.indices.size() * sizeof(int));
		changedBuffers = true;
	}
	else {
		meshData.indices.updateData(mesh.indices.data(), (int)mesh.indices.size() * sizeof(int), 0);
	}

	meshData.mesh.enabled = mesh.enabled;

	if (changedBuffers) {

		BDSegmentLayout s1{}, s2{};
		s1.buffer = &meshData.vertices;
		s1.byteOffset = 0;
		s1.byteStride = sizeof(Vertex);
		s1.component = BDComponent::VEC3;

		s2.buffer = &meshData.vertices;
		s2.byteOffset = sizeof(glm::vec3);
		s2.byteStride = sizeof(Vertex);
		s2.component = BDComponent::VEC2;

		meshData.descriptor.setLayout({ s1,s2 }, meshData.indices);
	}
}
void MeshRenderer::removeMesh(MeshID meshID) {
	this->meshData.erase(meshID);
}
