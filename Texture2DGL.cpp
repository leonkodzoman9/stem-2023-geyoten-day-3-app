#include "Texture2DGL.hpp"

#include "BufferGL.hpp"



Texture2DGL::Texture2DGL() {

	glCreateTextures(GL_TEXTURE_2D, 1, &this->ID);
}
Texture2DGL& Texture2DGL::operator=(Texture2DGL&& texture) noexcept {

	if (this != &texture) {
		std::swap(this->ID, texture.ID);
	}

	return *this;
}
Texture2DGL::~Texture2DGL() {

	glDeleteTextures(1, &this->ID);
}

void Texture2DGL::allocateStorage(glm::ivec2 size, TexturePixelFormat format, int mipCount = 1) {
		
	glTextureStorage2D(this->ID, mipCount, (uint32_t)format, size.x, size.y);
}
void Texture2DGL::updateData(void* data, glm::ivec2 offset, glm::ivec2 size, DataFormat format, DataType type, int mipmap = 0) {

	glTextureSubImage2D(this->ID, mipmap, offset.x, offset.y, size.x, size.y, (uint32_t)format, (uint32_t)type, data);
}
void Texture2DGL::updateData(BufferGL& buffer, glm::ivec2 offset, glm::ivec2 size, DataFormat format, DataType type, int mipmap) {

	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer.getID());
	glTextureSubImage2D(this->ID, mipmap, offset.x, offset.y, size.x, size.y, (uint32_t)format, (uint32_t)type, nullptr);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}
void Texture2DGL::downloadData(BufferGL& buffer, glm::ivec2 offset, glm::ivec2 size, DataFormat format, DataType type, int mipmap) {

	glBindBuffer(GL_PIXEL_PACK_BUFFER, buffer.getID());
	glGetTextureSubImage(this->ID, mipmap, offset.x, offset.y, 0, size.x, size.y, 1, (uint32_t)format, (uint32_t)type, size.x * size.y * sizeof(glm::u8vec4), nullptr);
	glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
}

void Texture2DGL::setParameter(TextureParameterName name, TextureParameterValue value) {

	glTextureParameteri(this->ID, (uint32_t)name, (uint32_t)value);
}

void Texture2DGL::generateMipmaps() {
	glGenerateTextureMipmap(this->ID);
}

const uint32_t Texture2DGL::getID() const {
	return this->ID;
}

void Texture2DGL::getImageData(void* destination, glm::ivec2 size) const {
	glGetTextureImage(this->ID, 0, GL_RGBA, GL_UNSIGNED_BYTE, size.x * size.y * sizeof(glm::u8vec4), destination);
}
