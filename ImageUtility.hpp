#pragma once

#include "Includes.hpp"

#include "Image.hpp"



enum class SamplingMode {
	NEAREST,
	LINEAR
};

namespace ImageUtility {

	ImageRGBA8 load(std::string path);
	void savePNG(std::string path, const ImageRGBA8& image);
	void saveJPG(std::string path, const ImageRGBA8& image, int quality = 90);
	void saveBMP(std::string path, const ImageRGBA8& image);

	template <class PixelType, int Channels>
	void flipVertically(Image<PixelType, Channels>& image) {

		for (int i = 0; i < image.size.y / 2; i++) {
			for (int j = 0; j < image.size.x; j++) {
				std::swap(image.at(i, j), image.at(image.size.y - 1 - i, j));
			}
		}
	}
	template <class PixelType, int Channels>
	void flipHorizontally(Image<PixelType, Channels>& image) {

		for (int i = 0; i < image.size.y; i++) {
			for (int j = 0; j < image.size.x / 2; j++) {
				std::swap(image.at(i, j), image.at(i, image.size.x - 1 - j));
			}
		}
	}

	template <class PixelType, int Channels>
	Image<PixelType, Channels> resize(const Image<PixelType, Channels>& image, glm::ivec2 newSize, SamplingMode samplingMode) {

		using IType = glm::vec<Channels, float>;

		float scaleX = image.size.x / (float)newSize.x;
		float scaleY = image.size.y / (float)newSize.y;

		int samplesX = std::ceil(scaleX);
		int samplesY = std::ceil(scaleY);

		Image<PixelType, Channels> newImage;

		if (samplingMode == SamplingMode::NEAREST) {
			newImage.size = newSize;
			newImage.data.resize(newImage.size.x * newImage.size.y);
			for (int y = 0; y < newImage.size.y; y++) {
				for (int x = 0; x < newImage.size.x; x++) {
					newImage.at(y, x) = image.at(scaleY * y, scaleX * x);
				}
			}
		}
		else {

			Image<PixelType, Channels> tempImage;
			tempImage.size = glm::ivec2(newSize.x, image.size.y);
			tempImage.data.resize(tempImage.size.x * tempImage.size.y);

			for (int y = 0; y < tempImage.size.y; y++) {
				for (int x = 0; x < tempImage.size.x; x++) {

					if (scaleX <= 1) {
						float point = std::max(scaleX * (x + 0.5f) - 0.5f, 0.0f);
						float diff = point - int(point);
						tempImage.at(y, x) = IType(image.at(y, point)) * (1 - diff) + IType(image.at(y, std::min<float>(point + 1, image.size.x - 1))) * diff;
					}
					else {

						float pointMin = scaleX * x;
						float pointMax = scaleX * (x + 1);

						IType pixel(0);
						for (float point = pointMin; point < pointMax; point += 1) {
							float diff = point - int(point);
							IType sample = IType(image.at(y, point)) * (1 - diff) + IType(image.at(y, std::min<float>(point + 1, image.size.x - 1))) * diff;
							pixel += sample * (std::min(point + 1, pointMax) - point);
						}

						tempImage.at(y, x) = pixel / (pointMax - pointMin);
					}
				}
			}

			newImage.size = glm::ivec2(newSize.x, newSize.y);
			newImage.data.resize(newImage.size.x * newImage.size.y);

			for (int y = 0; y < newImage.size.y; y++) {
				for (int x = 0; x < newImage.size.x; x++) {

					if (scaleY <= 1) {
						float point = std::max(scaleY * (y + 0.5f) - 0.5f, 0.0f);
						float diff = point - int(point);
						newImage.at(y, x) = IType(tempImage.at(point, x)) * (1 - diff) + IType(tempImage.at(std::min<float>(point + 1, tempImage.size.y - 1), x)) * diff;
					}
					else {

						float pointMin = scaleY * y;
						float pointMax = scaleY * (y + 1);

						IType pixel(0);
						for (float point = pointMin; point < pointMax; point += 1) {
							float diff = point - int(point);
							IType sample = IType(tempImage.at(point, x)) * (1 - diff) + IType(tempImage.at(std::min<float>(point + 1, tempImage.size.y - 1), x)) * diff;
							pixel += sample * (std::min(point + 1, pointMax) - point);
						}

						newImage.at(y, x) = pixel / (pointMax - pointMin);
					}
				}
			}
		}

		return newImage;
	}

	Image<uint8_t, 1> generateSDF(const Image<uint8_t, 1>& image, float spread);
}
