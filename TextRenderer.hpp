#pragma once

#include "Includes.hpp"

#include "ShaderGL.hpp"
#include "BufferGL.hpp"
#include "BufferDescriptorGL.hpp"
#include "Texture2DGL.hpp"
#include "Font.hpp"
#include "TextAlignment.hpp"



using TextID = uint32_t;



struct TextInformation {

	std::u32string text = U"None";
	std::string font = "consola";

	glm::vec3 position = glm::vec3(0);
	float height = 32;
	float letterSpacing = -10;

	AlignmentVertical alignmentVertical = AlignmentVertical::MIDDLE;
	AlignmentHorizontal alignmentHorizontal = AlignmentHorizontal::MIDDLE;

	glm::vec4 colorFill = glm::vec4(0.1, 0.1, 0.1, 1);
	glm::vec4 colorOutline = glm::vec4(1);

	float outlineMin1 = 0.1;
	float outlineMax1 = 0.2;
	float outlineMin2 = 0.4;
	float outlineMax2 = 0.5;

	bool enabled = true;
};


struct TextData {

	BufferGL charactersBuffer;
	BufferDescriptorGL descriptor;
	
	TextInformation textInformation;
};



class TextRenderer {

private:

	ShaderGL shaderText2D;
	
	UniformLocation locationColorFill;
	UniformLocation locationColorOutline;
	UniformLocation locationMin1;
	UniformLocation locationMin2;
	UniformLocation locationMax1;
	UniformLocation locationMax2;

	std::unordered_map<TextID, TextData> textData;
	std::unordered_map<std::string, Font> fonts;

public:

	TextRenderer();

	void setOrthographic(glm::mat4 orthographic);

	void render();

	void updateText(TextID entity, TextInformation textInformation);
	void removeText(TextID entity);
};



