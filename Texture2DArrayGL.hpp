#pragma once

#include "Includes.hpp"

#include "PixelFormat.hpp"
#include "TextureParameter.hpp"



using Texture2DArrayID = uint32_t;



class Texture2DArrayGL {

private:

    Texture2DArrayID ID = 0;

public:

    Texture2DArrayGL();
    ~Texture2DArrayGL();

    Texture2DArrayGL(const Texture2DArrayGL& rhs) = delete;
    Texture2DArrayGL& operator=(const Texture2DArrayGL& rhs) = delete;
    Texture2DArrayGL(Texture2DArrayGL&& rhs) noexcept = delete;
    Texture2DArrayGL& operator=(Texture2DArrayGL&& texture) noexcept;

    void allocateStorage(glm::ivec3 size, TexturePixelFormat format, int mipCount);
    void updateData(void* data, glm::ivec3 offset, glm::ivec3 size, DataFormat format, DataType type, int mipmap);
    void copyFrom(Texture2DArrayGL& texture, glm::ivec3 readOffset, glm::ivec3 writeOffset, glm::ivec3 size, int mipmap);

    void setParameter(TextureParameterName name, TextureParameterValue value);

    void generateMipmaps();

    const uint32_t getID() const;
    void getImageData(void* destination, glm::ivec3 size) const;
};





