#include "Texture2DArrayGL.hpp"



Texture2DArrayGL::Texture2DArrayGL() {

	glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &this->ID);
}
Texture2DArrayGL& Texture2DArrayGL::operator=(Texture2DArrayGL&& texture) noexcept {

	if (this != &texture) {
		std::swap(this->ID, texture.ID);
	}

	return *this;
}
Texture2DArrayGL::~Texture2DArrayGL() {

	glDeleteTextures(1, &this->ID);
}

void Texture2DArrayGL::allocateStorage(glm::ivec3 size, TexturePixelFormat format, int mipCount) {

	glTextureStorage3D(this->ID, mipCount, (uint32_t)format, size.x, size.y, size.z);
}
void Texture2DArrayGL::updateData(void* data, glm::ivec3 offset, glm::ivec3 size, DataFormat format, DataType type, int mipmap) {

	glTextureSubImage3D(this->ID, mipmap, offset.x, offset.y, offset.z, size.x, size.y, size.z, (uint32_t)format, (uint32_t)type, data);
}
void Texture2DArrayGL::copyFrom(Texture2DArrayGL& texture, glm::ivec3 readOffset, glm::ivec3 writeOffset, glm::ivec3 size, int mipmap) {

	glCopyImageSubData(
		texture.ID,
		GL_TEXTURE_2D_ARRAY,
		mipmap,
		readOffset.x,
		readOffset.y,
		readOffset.z,
		this->ID,
		GL_TEXTURE_2D_ARRAY,
		mipmap,
		writeOffset.x,
		writeOffset.y,
		writeOffset.z,
		size.x,
		size.y,
		size.z
	);
}

void Texture2DArrayGL::setParameter(TextureParameterName name, TextureParameterValue value) {

	glTextureParameteri(this->ID, (uint32_t)name, (uint32_t)value);
}

void Texture2DArrayGL::generateMipmaps() {
	glGenerateTextureMipmap(this->ID);
}

const uint32_t Texture2DArrayGL::getID() const {
	return this->ID;
}

void Texture2DArrayGL::getImageData(void* destination, glm::ivec3 size) const {
	glGetTextureImage(this->ID, 0, GL_RGBA, GL_UNSIGNED_BYTE, size.x * size.y * size.z * sizeof(glm::u8vec4), destination);
}
