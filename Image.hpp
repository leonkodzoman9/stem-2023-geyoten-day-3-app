#pragma once

#include "Includes.hpp"



template <class PixelType, int Channels>
struct Image {

    glm::ivec2 size = glm::ivec2(0);
    std::vector<glm::vec<Channels, PixelType>> data;

    Image() = default;
    Image(glm::ivec2 size) {
        this->size = size;
        this->data.resize(size.x * size.y, glm::vec<Channels, PixelType>(0));
    }

    glm::vec<Channels, PixelType>& at(int row, int col) {
        return this->data[row * this->size.x + col];
    }
    const glm::vec<Channels, PixelType>& at(int row, int col) const {
        return this->data[row * this->size.x + col];
    }

    glm::vec<Channels, PixelType>& at(glm::ivec2 position) {
        return this->data[position.y * this->size.x + position.x];
    }
    const glm::vec<Channels, PixelType>& at(glm::ivec2 position) const {
        return this->data[position.y * this->size.x + position.x];
    }
};

using ImageRGBA8 = Image<glm::u8, 4>;
