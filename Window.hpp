#pragma once

#include "Includes.hpp"



enum class CursorMode {
	NORMAL = GLFW_CURSOR_NORMAL,
	HIDDEN = GLFW_CURSOR_HIDDEN,
	DISABLED = GLFW_CURSOR_DISABLED
};

enum class VSyncMode : int {
	NONE = 0,
	FULL = 1,
	HALF = 2
};



struct Monitor {
	glm::ivec2 size;
	int refreshRate;
};



class Window {

private:

	glm::ivec2 size;
	std::string title;

	GLFWwindow* window = nullptr;

public:

	static Window& getInstance();

	void initialize(glm::ivec2 windowSize, std::string_view windowName = "Window");

	void setVSync(VSyncMode mode);

	GLFWwindow* getWindow();

	glm::ivec2 getSize() const;
	std::string getTitle() const;
	double getAspectRatio() const;
	void setTitle(std::string_view windowName);

	void setCursorMode(CursorMode mode);

private:

	Window();
	~Window();
};
