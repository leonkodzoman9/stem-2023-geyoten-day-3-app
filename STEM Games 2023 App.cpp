#include "Includes.hpp"

#include "Window.hpp"
#include "Mouse.hpp"
#include "Keyboard.hpp"

#include "ShaderGL.hpp"
#include "BufferGL.hpp"
#include "BufferDescriptorGL.hpp"
#include "Texture2DGL.hpp"
#include "Font.hpp"
#include "TextRenderer.hpp"
#include "MeshRenderer.hpp"

#include "Image.hpp"
#include "ImageUtility.hpp"
#include "Mesh.hpp"
#include "Utility.hpp"

#include "RSJP.hpp"



int64_t MeshIndex = 1;
int64_t TextIndex = 1;


constexpr glm::ivec2 wSize = glm::ivec2(1600, 900);



void GLAPIENTRY
MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {

	if (type != GL_DEBUG_TYPE_ERROR) { return; }

	fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
		type, severity, message);
}



struct Sample {
	std::string familyName;
	std::string productName;
	std::string name;
	int ID;

	void parseFromJson(RSJresource& json) {

		this->familyName = json["FamilyName"].as_str();
		this->productName = json["ProductName"].as_str();
		this->name = json["Name"].as_str();
		this->ID = json["Id"].as<int>();
	}
};
struct InputCondition {
	
	std::string parameter;
	double min;
	double typical;
	double max;
	double timeBetweenPoints;
	int ID;

	void parseFromJson(RSJresource& json) {

		this->parameter = json["Parameter"].as_str();
		this->min = json["Min"].as<double>();
		this->typical = json["Typical"].as<double>();
		this->max = json["Max"].as<double>();
		this->timeBetweenPoints = json["TimeBetweenPoints"].as<double>();
		this->ID = json["Id"].as<int>();
	}
};
struct Project {
	std::string name;
	std::vector<Sample> samples;
	std::vector<InputCondition> inputConditions;
	int ID;

	void parseFromJson(RSJresource& json) {

		this->name = json["Name"].as_str();
		
		int i = 0;
		for (auto iter = json["Samples"].as_array().begin(); iter != json["Samples"].as_array().end(); iter++) {
			Sample sample;
			sample.parseFromJson(json["Samples"][i++]);
			this->samples.push_back(sample);
		}

		i = 0;
		for (auto iter = json["InputConditions"].as_array().begin(); iter != json["InputConditions"].as_array().end(); iter++) {
			InputCondition inputCondition;
			inputCondition.parseFromJson(json["InputConditions"][i++]);
			this->inputConditions.push_back(inputCondition);
		}

		this->ID = json["Id"].as<int>();
	}
};

struct TestPoint {
	double value;
	std::string unit;

	void parseFromJson(RSJresource& json) {
		this->value = json["Value"].as<double>();
		this->unit = json["Unit"].as_str();
	}
};
struct Collection {

	int inputConditionID;
	std::vector<int> samples;
	std::vector<TestPoint> testPoints;

	int ID;

	void parseFromJson(RSJresource& json) {

		this->inputConditionID = json["InputConditionId"].as<int>();

		int i = 0;
		for (auto iter = json["SampleIds"].as_array().begin(); iter != json["SampleIds"].as_array().end(); iter++) {
			this->samples.push_back(json["SampleIds"][i++].as<int>());
		}

		i = 0;
		for (auto iter = json["TestPoints"].as_array().begin(); iter != json["TestPoints"].as_array().end(); iter++) {

			TestPoint testPoint;
			testPoint.parseFromJson(json["TestPoints"][i++]);
			this->testPoints.push_back(testPoint);
		}

		this->ID = json["Id"].as<int>();
	}
};
struct TestPointCollection {
	std::vector<Collection> collections;

	void parseFromJson(RSJresource& json) {

		int i = 0;
		for (auto iter = json.as_array().begin(); iter != json.as_array().end(); iter++) {
			Collection collection;
			collection.parseFromJson(json[i++]);
			this->collections.push_back(collection);
		}
	}
};

struct ProjectAndCollections {

	Project project;
	TestPointCollection collections;

	void parseJson(std::string path) {

		std::ifstream file(path);
		std::stringstream ss;
		ss << file.rdbuf();
		std::string s = ss.str();
		file.close();

		RSJresource json(s);

		this->project.parseFromJson(json["Project"]);
		this->collections.parseFromJson(json["TestPointCollections"]);
	}
};



Mesh createQuad(glm::vec3 center, glm::vec2 size) {

	glm::vec3 bl = center - glm::vec3(size / 2.0f, 0);
	glm::vec3 tr = center + glm::vec3(size / 2.0f, 0);

	Mesh mesh;
	mesh.vertices.push_back({ glm::vec3(bl.x, bl.y, bl.z), glm::vec2(0, 0) });
	mesh.vertices.push_back({ glm::vec3(tr.x, bl.y, bl.z), glm::vec2(1, 0) });
	mesh.vertices.push_back({ glm::vec3(tr.x, tr.y, bl.z), glm::vec2(1, 1) });
	mesh.vertices.push_back({ glm::vec3(bl.x, tr.y, bl.z), glm::vec2(0, 1) });

	mesh.indices = { 0,1,2,0,2,3 };

	return mesh;
}

std::pair<std::vector<Mesh>, std::vector<glm::vec3>> createRow(const std::vector<int>& relativeSizes, float padding, glm::vec3 bl, glm::vec3 size) {

	std::vector<Mesh> meshes;
	std::vector<glm::vec3> centers;

	int sum = std::reduce(relativeSizes.begin(), relativeSizes.end());

	int offset = 0;
	for (int i = 0; i < relativeSizes.size(); i++) {
		float percentOffset = offset / float(sum);
		float percentSize = relativeSizes[i] / float(sum);

		glm::vec3 bbl = bl + size * percentOffset;
		glm::vec3 btr = bl + size * percentOffset + size * percentSize;
		bbl.y = bl.y - size.y / 2;
		btr.y = bl.y + size.y / 2;

		bbl += padding;
		btr -= padding;

		meshes.push_back(createQuad((bbl + btr) / 2.0f, btr - bbl));
		centers.push_back((bbl + btr) / 2.0f);

		offset += relativeSizes[i];
	}

	return { meshes, centers };
}



std::u32string stripTrailingZeros(std::u32string s) {

	while (s.back() == U'0') { s.pop_back(); }
	if (s.back() == U'.') { s.pop_back(); }

	return s;
}



struct SampleTable {

	std::vector<TextInformation> texts;
	std::vector<TextID> textIDs;

	std::vector<Mesh> meshes;
	std::vector<MeshID> meshIDs;

	void create(Project& project) {

		{
			Mesh mesh = createQuad(glm::vec3(wSize.x / 2, wSize.y - 100, 0), glm::vec2(1000, 50) - 4.0f);
			mesh.color = glm::vec4(0.4, 0.7, 0.8, 1);
			this->meshes.push_back(mesh);
			this->meshIDs.push_back(MeshIndex++);

			TextInformation text;
			text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
			text.alignmentVertical = AlignmentVertical::MIDDLE;
			text.text = U"Samples";
			text.position = glm::vec3(wSize.x / 2, wSize.y - 100, 0.1);
			text.height = 40;
			text.outlineMin1 = 0.4;
			text.outlineMax1 = 0.5;
			text.outlineMin2 = 0.5;
			text.outlineMax2 = 0.5;
			text.colorFill = glm::vec4(0, 0, 0, 1);
			text.colorOutline = glm::vec4(0, 0, 0, 1);

			this->texts.push_back(text);
			this->textIDs.push_back(TextIndex++);
		}

		auto [rows, centers] = createRow({ 1,3,3,2 }, 2, glm::vec3(wSize.x / 2 - 500, wSize.y - 150, 0), glm::vec3(1000, 50, 0));
		std::vector<std::u32string> texts = { U"ID", U"Family name", U"Product name", U"Name" };
		{
			for (int i = 0; i < rows.size(); i++) {

				Mesh& mesh = rows[i];
				mesh.color = glm::vec4(0.7, 0.9, 1, 1);
				this->meshes.push_back(mesh);
				this->meshIDs.push_back(MeshIndex++);

				TextInformation text;
				text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
				text.alignmentVertical = AlignmentVertical::MIDDLE;
				text.text = texts[i];
				text.height = 40;
				text.outlineMin1 = 0.4;
				text.outlineMax1 = 0.5;
				text.outlineMin2 = 0.5;
				text.outlineMax2 = 0.5;
				text.position = glm::vec3(glm::vec2(centers[i]), 0.1);
				text.colorFill = glm::vec4(0, 0, 0, 1);
				text.colorOutline = glm::vec4(0, 0, 0, 1);

				this->texts.push_back(text);
				this->textIDs.push_back(TextIndex++);
			}
		}

		for (int s = 0; s < project.samples.size(); s++) {

			auto [rows, centers] = createRow({ 1,3,3,2 }, 2, glm::vec3(wSize.x / 2 - 500, wSize.y - 150 - 50 * (s + 1), 0), glm::vec3(1000, 50, 0));
			{

				std::vector<std::u32string> texts;
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(project.samples[s].ID)));
				texts.push_back(Utility::UTF8ToUTF32(project.samples[s].familyName));
				texts.push_back(Utility::UTF8ToUTF32(project.samples[s].productName));
				texts.push_back(Utility::UTF8ToUTF32(project.samples[s].name));

				for (int i = 0; i < rows.size(); i++) {

					Mesh& mesh = rows[i];
					this->meshes.push_back(mesh);
					this->meshIDs.push_back(MeshIndex++);

					TextInformation text;
					text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
					text.alignmentVertical = AlignmentVertical::MIDDLE;
					if (texts[i][0] == U'"') {
						text.text = texts[i].substr(1, texts[i].size() - 2);
					}
					else {
						text.text = texts[i];
					}
					text.height = 40;
					text.outlineMin1 = 0.4;
					text.outlineMax1 = 0.5;
					text.outlineMin2 = 0.5;
					text.outlineMax2 = 0.5;
					text.position = glm::vec3(glm::vec2(centers[i]), 0.1);
					text.colorFill = glm::vec4(0, 0, 0, 1);
					text.colorOutline = glm::vec4(0, 0, 0, 1);

					this->texts.push_back(text);
					this->textIDs.push_back(TextIndex++);
				}
			}
		}
	}

	void toggle() {

		for (int i = 0; i < meshes.size(); i++) {
			this->meshes[i].enabled ^= true;
		}
		for (int i = 0; i < texts.size(); i++) {
			this->texts[i].enabled ^= true;
		}
	}

	int getClickedSample(glm::dvec2 pos) {

		pos.y = wSize.y - pos.y;

		for (int i = 0; i < this->meshes.size(); i++) {
			if (pos.y > this->meshes[i].vertices[0].position.y && pos.y < this->meshes[i].vertices[2].position.y) {
				return (i - 1 - 4) / 4;
			}
		}

		return -1;
	}
};

struct InputConditionsTable {

	std::vector<TextInformation> texts;
	std::vector<TextID> textIDs;

	std::vector<Mesh> meshes;
	std::vector<MeshID> meshIDs;

	void create(Project& project) {

		{
			Mesh mesh = createQuad(glm::vec3(wSize.x / 2, wSize.y - 100, 0), glm::vec2(1000, 50) - 4.0f);
			mesh.color = glm::vec4(0.4, 0.7, 0.8, 1);
			this->meshes.push_back(mesh);
			this->meshIDs.push_back(MeshIndex++);

			TextInformation text;
			text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
			text.alignmentVertical = AlignmentVertical::MIDDLE;
			text.text = U"Input Conditions";
			text.position = glm::vec3(wSize.x / 2, wSize.y - 100, 0.1);
			text.height = 40;
			text.outlineMin1 = 0.4;
			text.outlineMax1 = 0.5;
			text.outlineMin2 = 0.5;
			text.outlineMax2 = 0.5;
			text.colorFill = glm::vec4(0, 0, 0, 1);
			text.colorOutline = glm::vec4(0, 0, 0, 1);

			this->texts.push_back(text);
			this->textIDs.push_back(TextIndex++);
		}

		auto [rows, centers] = createRow({ 1,5,4,4,4 }, 2, glm::vec3(wSize.x / 2 - 500, wSize.y - 150, 0), glm::vec3(1000, 50, 0));
		std::vector<std::u32string> texts = { U"ID", U"Parameter", U"Minimum", U"Maximum", U"Time between points"};
		{
			for (int i = 0; i < rows.size(); i++) {

				Mesh& mesh = rows[i];
				mesh.color = glm::vec4(0.7, 0.9, 1, 1);
				this->meshes.push_back(mesh);
				this->meshIDs.push_back(MeshIndex++);

				TextInformation text;
				text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
				text.alignmentVertical = AlignmentVertical::MIDDLE;
				text.text = texts[i];
				text.height = 28;
				text.outlineMin1 = 0.4;
				text.outlineMax1 = 0.5;
				text.outlineMin2 = 0.5;
				text.outlineMax2 = 0.5;
				text.position = glm::vec3(glm::vec2(centers[i]), 0.1);
				text.colorFill = glm::vec4(0, 0, 0, 1);
				text.colorOutline = glm::vec4(0, 0, 0, 1);

				this->texts.push_back(text);
				this->textIDs.push_back(TextIndex++);
			}
		}

		for (int s = 0; s < project.inputConditions.size(); s++) {

			auto [rows, centers] = createRow({ 1,5,4,4,4 }, 2, glm::vec3(wSize.x / 2 - 500, wSize.y - 200 - 40 * s, 0), glm::vec3(1000, 40, 0));
			{

				std::vector<std::u32string> texts;
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(project.inputConditions[s].ID)));
				texts.push_back(Utility::UTF8ToUTF32(project.inputConditions[s].parameter));
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(project.inputConditions[s].min)));
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(project.inputConditions[s].max)));
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(project.inputConditions[s].timeBetweenPoints)));

				for (auto& text : texts) {
					while (text.back() == U'0') { text.pop_back(); }
					if (text.back() == U'.') {
						text.pop_back();
					}
				}

				for (int i = 0; i < rows.size(); i++) {

					Mesh& mesh = rows[i];
					this->meshes.push_back(mesh);
					this->meshIDs.push_back(MeshIndex++);

					TextInformation text;
					text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
					text.alignmentVertical = AlignmentVertical::MIDDLE;
					if (texts[i][0] == U'"') {
						text.text = texts[i].substr(1, texts[i].size() - 2);
					}
					else {
						text.text = texts[i];
					}
					text.height = 28;
					text.outlineMin1 = 0.4;
					text.outlineMax1 = 0.5;
					text.outlineMin2 = 0.5;
					text.outlineMax2 = 0.5;
					text.position = glm::vec3(glm::vec2(centers[i]), 0.1);
					text.colorFill = glm::vec4(0, 0, 0, 1);
					text.colorOutline = glm::vec4(0, 0, 0, 1);

					this->texts.push_back(text);
					this->textIDs.push_back(TextIndex++);
				}
			}
		}
	}

	void toggle() {

		for (int i = 0; i < meshes.size(); i++) {
			this->meshes[i].enabled ^= true;
		}
		for (int i = 0; i < texts.size(); i++) {
			this->texts[i].enabled ^= true;
		}
	}
};

struct TestPointCollectionsTable {

	std::vector<TextInformation> texts;
	std::vector<TextID> textIDs;

	std::vector<Mesh> meshes;
	std::vector<MeshID> meshIDs;

	void create(TestPointCollection& tpc) {

		{
			Mesh mesh = createQuad(glm::vec3(wSize.x / 2, wSize.y - 100, 0), glm::vec2(1000, 50) - 4.0f);
			mesh.color = glm::vec4(0.4, 0.7, 0.8, 1);
			this->meshes.push_back(mesh);
			this->meshIDs.push_back(MeshIndex++);

			TextInformation text;
			text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
			text.alignmentVertical = AlignmentVertical::MIDDLE;
			text.text = U"Test point collections";
			text.position = glm::vec3(wSize.x / 2, wSize.y - 100, 0.1);
			text.height = 40;
			text.outlineMin1 = 0.4;
			text.outlineMax1 = 0.5;
			text.outlineMin2 = 0.5;
			text.outlineMax2 = 0.5;
			text.colorFill = glm::vec4(0, 0, 0, 1);
			text.colorOutline = glm::vec4(0, 0, 0, 1);

			this->texts.push_back(text);
			this->textIDs.push_back(TextIndex++);
		}

		auto [rows, centers] = createRow({ 1,3,3,5 }, 2, glm::vec3(wSize.x / 2 - 500, wSize.y - 150, 0), glm::vec3(1000, 50, 0));
		std::vector<std::u32string> texts = { U"ID", U"Input Condition ID", U"Sample ID", U"Test Points" };
		{
			for (int i = 0; i < rows.size(); i++) {

				Mesh& mesh = rows[i];
				mesh.color = glm::vec4(0.7, 0.9, 1, 1);
				this->meshes.push_back(mesh);
				this->meshIDs.push_back(MeshIndex++);

				TextInformation text;
				text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
				text.alignmentVertical = AlignmentVertical::MIDDLE;
				text.text = texts[i];
				text.height = 28;
				text.outlineMin1 = 0.4;
				text.outlineMax1 = 0.5;
				text.outlineMin2 = 0.5;
				text.outlineMax2 = 0.5;
				text.position = glm::vec3(glm::vec2(centers[i]), 0.1);
				text.colorFill = glm::vec4(0, 0, 0, 1);
				text.colorOutline = glm::vec4(0, 0, 0, 1);

				this->texts.push_back(text);
				this->textIDs.push_back(TextIndex++);
			}
		}

		for (int s = 0; s < tpc.collections.size(); s++) {

			auto [rows, centers] = createRow({ 1,3,3,5 }, 2, glm::vec3(wSize.x / 2 - 500, wSize.y - 200 - 40 * s, 0), glm::vec3(1000, 40, 0));
			{

				std::vector<std::u32string> texts;
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(tpc.collections[s].ID)));
				texts.push_back(Utility::UTF8ToUTF32(std::to_string(tpc.collections[s].inputConditionID)));

				std::u32string s1;
				for (int i = 0; i < tpc.collections[s].samples.size(); i++) {
					s1 += Utility::UTF8ToUTF32(std::to_string(tpc.collections[s].samples[i]) + ",");
				}
				s1.pop_back();
				texts.push_back(s1);


				std::u32string s2;
				for (int i = 0; i < tpc.collections[s].testPoints.size(); i++) {
					s2 += stripTrailingZeros(Utility::UTF8ToUTF32(std::to_string(tpc.collections[s].testPoints[i].value))) + U",";
				}
				s2.pop_back();


				texts.push_back(s2);



				for (int i = 0; i < rows.size(); i++) {

					Mesh& mesh = rows[i];
					this->meshes.push_back(mesh);
					this->meshIDs.push_back(MeshIndex++);

					TextInformation text;
					text.alignmentHorizontal = AlignmentHorizontal::MIDDLE;
					text.alignmentVertical = AlignmentVertical::MIDDLE;
					text.text = texts[i];
					text.height = 28;
					text.outlineMin1 = 0.4;
					text.outlineMax1 = 0.5;
					text.outlineMin2 = 0.5;
					text.outlineMax2 = 0.5;
					text.position = glm::vec3(glm::vec2(centers[i]), 0.1);
					text.colorFill = glm::vec4(0, 0, 0, 1);
					text.colorOutline = glm::vec4(0, 0, 0, 1);

					this->texts.push_back(text);
					this->textIDs.push_back(TextIndex++);
				}
			}
		}
	}

	void toggle() {

		for (int i = 0; i < meshes.size(); i++) {
			this->meshes[i].enabled ^= true;
		}
		for (int i = 0; i < texts.size(); i++) {
			this->texts[i].enabled ^= true;
		}
	}
};





int main() {

	Window& window = Window::getInstance();
	window.initialize(wSize, "App");
	Mouse& mouse = Mouse::getInstance();
	Keyboard& keyboard = Keyboard::getInstance();

	// During init, enable debug output
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ShaderGL shader;
	shader.addShaderStage("shaderMesh", ShaderStage::VERTEX);
	shader.addShaderStage("shaderMesh", ShaderStage::FRAGMENT);
	shader.createProgram();


	ImageRGBA8 image = ImageUtility::load("Textures/button.png");

	Texture2DGL texture;
	texture.allocateStorage(image.size, TexturePixelFormat::RGBA8, 1);
	texture.updateData(image.data.data(), glm::ivec2(0), image.size, DataFormat::RGBA, DataType::UNSIGNED_BYTE, 0);
	texture.setParameter(TextureParameterName::MIN_FILTER, TextureParameterValue::LINEAR);
	texture.setParameter(TextureParameterName::MAG_FILTER, TextureParameterValue::LINEAR);



	ProjectAndCollections pac;
	pac.parseJson("Datasets/L1.json");


	SampleTable table1;
	InputConditionsTable table2;
	TestPointCollectionsTable table3;

	table1.create(pac.project);
	table2.create(pac.project);
	table3.create(pac.collections);
	table2.toggle();
	table3.toggle();

	int selectedTable = 0;



	TextInformation textInfo;
	textInfo.position = glm::vec3(100, 100, 0);
	textInfo.colorOutline = glm::vec4(1, 0, 0, 1);

	TextRenderer textRenderer;
	MeshRenderer meshRenderer;

	while (!glfwWindowShouldClose(window.getWindow())) {

		mouse.update();
		keyboard.update();

		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.2, 0.2, 0.2, 1);

		glEnable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		
		glm::mat4 orthographic = glm::ortho<float>(0, wSize.x, 0, wSize.y, -1, 1);

		for (int i = 0; i < table1.meshIDs.size(); i++) {
			meshRenderer.updateMesh(table1.meshIDs[i], table1.meshes[i]);
		}
		for (int i = 0; i < table2.meshIDs.size(); i++) {
			meshRenderer.updateMesh(table2.meshIDs[i], table2.meshes[i]);
		}
		for (int i = 0; i < table3.meshIDs.size(); i++) {
			meshRenderer.updateMesh(table3.meshIDs[i], table3.meshes[i]);
		}

		meshRenderer.setOrthographic(orthographic);
		meshRenderer.render();

		for (int i = 0; i < table1.textIDs.size(); i++) {
			textRenderer.updateText(table1.textIDs[i], table1.texts[i]);
		}
		for (int i = 0; i < table2.textIDs.size(); i++) {
			textRenderer.updateText(table2.textIDs[i], table2.texts[i]);
		}
		for (int i = 0; i < table3.textIDs.size(); i++) {
			textRenderer.updateText(table3.textIDs[i], table3.texts[i]);
		}

		textRenderer.setOrthographic(orthographic);
		textRenderer.render();

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);

		glfwSwapBuffers(window.getWindow());

		if (keyboard.held(Key::LEFT_CONTROL) && keyboard.pressed(Key::Q)) {
			break;
		}

		if (keyboard.pressed(Key::RIGHT)) {
			if (selectedTable == 0) {
				table1.toggle();
			}
			else if (selectedTable == 1) {
				table2.toggle();
			}
			else if (selectedTable == 2) {
				table3.toggle();
			}

			selectedTable = (selectedTable + 1) % 3;

			if (selectedTable == 0) {
				table1.toggle();
			}
			else if (selectedTable == 1) {
				table2.toggle();
			}
			else if (selectedTable == 2) {
				table3.toggle();
			}
		}

		if (mouse.pressed(Button::MOUSE_LEFT)) {
			if (selectedTable == 0) {
				int clickedOn = table1.getClickedSample(mouse.position());
				
				if (clickedOn < 0) { continue; }

				int sampleID = pac.project.samples[clickedOn].ID;

				std::vector<std::string> parameters;
				std::vector<std::vector<TestPoint>> testPoints;

				for (int i = 0; i < pac.collections.collections.size(); i++) {

					auto& vec = pac.collections.collections[i].samples;

					if (std::find(vec.begin(), vec.end(), sampleID) != vec.end()) {
						testPoints.push_back(pac.collections.collections[i].testPoints);

						int icid = pac.collections.collections[i].inputConditionID;

						for (int j = 0; j < pac.project.inputConditions.size(); j++) {
							if (pac.project.inputConditions[j].ID == icid) {
								parameters.push_back(pac.project.inputConditions[j].parameter);
							}
						}
					}
				}

				std::ofstream file("params.csv");

				for (int i = 0; i < parameters.size(); i++) {

					file << parameters[i].substr(1, parameters[i].size() - 2);
					if (i == testPoints.size() - 1) {
						file << "\n";
					}
					else {
						file << ",";
					}
				}

				std::vector<int> indices(testPoints.size());
				while (indices.back() < testPoints.back().size()) {

					for (int i = 0; i < testPoints.size(); i++) {

						file << testPoints[i][indices[i]].value;
						if (i == testPoints.size() - 1) {
							file << "\n";
						}
						else {
							file << ",";
						}
					}

					indices[0]++;
					for (int i = 0; i < testPoints.size() - 1; i++) {
						if (indices[i] >= testPoints[i].size()) {
							indices[i] = 0;
							indices[i + 1]++;
						}
					}
				}

				file.close();
			}
		}
	}

	return 0;
}