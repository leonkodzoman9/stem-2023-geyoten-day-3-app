#include "TextRenderer.hpp"

#include "Window.hpp"
#include "BufferGL.hpp"
#include "BufferDescriptorGL.hpp"
#include "ShaderGL.hpp"
#include "Texture2DGL.hpp"
#include "Font.hpp"



struct Character2D {
	std::array<glm::vec4, 4> positions;
	std::array<glm::vec4, 4> uvs;
};



static const std::array<float, 3> OffsetAlignmentHorizontal = { 0, -0.5, -1 };
static const std::array<float, 3> OffsetAlignmentVertical = { -1, -0.5, 0 };

float getTextHeight(const std::u32string& text, Font& font, float scale) {

	float textHeight = 0;
	for (const char32_t& character : text) {
		if (!font.isLoaded(character)) { continue; }
		textHeight = std::max(textHeight, font.getSymbol(character).size.y * scale);
	}

	return textHeight;
}
float getTextWidth(const std::u32string& text, Font& font, float scale, float spacing) {

	float textWidth = 0;
	for (const char32_t& character : text) {
		if (!font.isLoaded(character)) { continue; }
		textWidth += (font.getSymbol(character).advance + spacing) * scale;
	}
	textWidth -= spacing * scale;

	return textWidth;
}



TextRenderer::TextRenderer() {

	this->shaderText2D.addShaderStage("shaderText", ShaderStage::VERTEX);
	this->shaderText2D.addShaderStage("shaderText", ShaderStage::FRAGMENT);
	this->shaderText2D.createProgram();

	this->locationColorFill = this->shaderText2D.getUniformLocation("colorFill");
	this->locationColorOutline = this->shaderText2D.getUniformLocation("colorOutline");
	this->locationMin1 = this->shaderText2D.getUniformLocation("min1");
	this->locationMin2 = this->shaderText2D.getUniformLocation("min2");
	this->locationMax1 = this->shaderText2D.getUniformLocation("max1");
	this->locationMax2 = this->shaderText2D.getUniformLocation("max2");
}

void TextRenderer::setOrthographic(glm::mat4 orthographic) {

	this->shaderText2D.use();
	this->shaderText2D.setMatrix4f("orthographic", orthographic);
}

void TextRenderer::render() {

	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);

	this->shaderText2D.use();

	for (const auto& [textID, textData] : this->textData) {

		if (!textData.textInformation.enabled) { continue; }

		Font& font = this->fonts[textData.textInformation.font];

		glBindTexture(GL_TEXTURE_2D_ARRAY, font.getAtlas().getID());
		
		this->shaderText2D.setVector4f(this->locationColorFill, textData.textInformation.colorFill);
		this->shaderText2D.setVector4f(this->locationColorOutline, textData.textInformation.colorOutline);
		this->shaderText2D.setFloat(this->locationMin1, textData.textInformation.outlineMin1);
		this->shaderText2D.setFloat(this->locationMin2, textData.textInformation.outlineMin2);
		this->shaderText2D.setFloat(this->locationMax1, textData.textInformation.outlineMax1);
		this->shaderText2D.setFloat(this->locationMax2, textData.textInformation.outlineMax2);
		this->shaderText2D.setVector2f("texSize", font.getAtlasSize());
		
		textData.charactersBuffer.bindToIndex(0, IndexType::SHADER_STORAGE);
		textData.descriptor.use();
		glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, textData.textInformation.text.size());
	}

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void TextRenderer::updateText(TextID textID, TextInformation textInformation) {

	if (textInformation.text.size() > 25) {
		textInformation.text = textInformation.text.substr(0, 25) + U"...";
	}

	if (this->fonts.find(textInformation.font) == this->fonts.end()) {
		this->fonts.emplace(textInformation.font, textInformation.font);
	}

	Font& font = this->fonts[textInformation.font];

	float scale = textInformation.height / font.getResolution();

	float textWidth = getTextWidth(textInformation.text, font, scale, textInformation.letterSpacing);
	float textHeight = getTextHeight(textInformation.text, font, scale);

	float offsetHorizontal = OffsetAlignmentHorizontal[(uint32_t)textInformation.alignmentHorizontal];
	float offsetVertical = OffsetAlignmentVertical[(uint32_t)textInformation.alignmentVertical];

	glm::vec2 offset = glm::vec2(textWidth, textHeight) * glm::vec2(offsetHorizontal, offsetVertical);

	float offsetX = 0;

	std::vector<Character2D> characters(textInformation.text.size());
	for (int i = 0; i < characters.size(); i++) {

		if (!font.isLoaded(textInformation.text[i])) { continue; }

		const Symbol& symbol = font.getSymbol(textInformation.text[i]);

		glm::vec2 position = glm::vec2(textInformation.position) + glm::vec2(offsetX, 0) + glm::vec2(0, symbol.bearing.y) * scale;

		characters[i].positions[0] = glm::vec4(position + glm::vec2(0, 0) * symbol.size * scale + offset, textInformation.position.z, 1);
		characters[i].positions[1] = glm::vec4(position + glm::vec2(1, 0) * symbol.size * scale + offset, textInformation.position.z, 1);
		characters[i].positions[2] = glm::vec4(position + glm::vec2(1, -1) * symbol.size * scale + offset, textInformation.position.z, 1);
		characters[i].positions[3] = glm::vec4(position + glm::vec2(0, -1) * symbol.size * scale + offset, textInformation.position.z, 1);

		characters[i].uvs[0] = glm::vec4(symbol.size * glm::vec2(0, 0), 0, symbol.layer);
		characters[i].uvs[1] = glm::vec4(symbol.size * glm::vec2(1, 0), 0, symbol.layer);
		characters[i].uvs[2] = glm::vec4(symbol.size * glm::vec2(1, 1), 0, symbol.layer);
		characters[i].uvs[3] = glm::vec4(symbol.size * glm::vec2(0, 1), 0, symbol.layer);

		offsetX += (symbol.advance + textInformation.letterSpacing) * scale;
	}

	if (this->textData.find(textID) == this->textData.end()) {
		this->textData[textID].charactersBuffer.allocateStorage(characters.data(), characters.size() * sizeof(Character2D));
	}
	else {
		if (this->textData[textID].textInformation.text.size() != characters.size()) {
			this->textData[textID].charactersBuffer = BufferGL();
			this->textData[textID].charactersBuffer.allocateStorage(nullptr, characters.size() * sizeof(Character2D));
		}
		this->textData[textID].charactersBuffer.updateData(characters.data(), characters.size() * sizeof(Character2D), 0);
	}

	this->textData[textID].textInformation = textInformation;
}
void TextRenderer::removeText(TextID textID) {
	this->textData.erase(textID);
}


