#pragma once

#include "Includes.hpp"

#include "Texture2DGL.hpp"



struct Vertex {
	glm::vec3 position;
	glm::vec2 uv;
};

struct Mesh {
	std::vector<Vertex> vertices;
	std::vector<int> indices;

	std::shared_ptr<Texture2DGL> texture;
	glm::vec4 color = glm::vec4(1);
	bool enabled = true;
};


