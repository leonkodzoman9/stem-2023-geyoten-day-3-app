#include "Keyboard.hpp"

#include "Window.hpp"



uint32_t getKeyValue(Key key) {

	switch (key) {
	case Key::A:				return GLFW_KEY_A;
	case Key::B:				return GLFW_KEY_B;
	case Key::C:				return GLFW_KEY_C;
	case Key::D:				return GLFW_KEY_D;
	case Key::E:				return GLFW_KEY_E;
	case Key::F:				return GLFW_KEY_F;
	case Key::G:				return GLFW_KEY_G;
	case Key::H:				return GLFW_KEY_H;
	case Key::I:				return GLFW_KEY_I;
	case Key::J:				return GLFW_KEY_J;
	case Key::K:				return GLFW_KEY_K;
	case Key::L:				return GLFW_KEY_L;
	case Key::M:				return GLFW_KEY_M;
	case Key::N:				return GLFW_KEY_N;
	case Key::O:				return GLFW_KEY_O;
	case Key::P:				return GLFW_KEY_P;
	case Key::Q:				return GLFW_KEY_Q;
	case Key::R:				return GLFW_KEY_R;
	case Key::S:				return GLFW_KEY_S;
	case Key::T:				return GLFW_KEY_T;
	case Key::U:				return GLFW_KEY_U;
	case Key::V:				return GLFW_KEY_V;
	case Key::W:				return GLFW_KEY_W;
	case Key::X:				return GLFW_KEY_X;
	case Key::Y:				return GLFW_KEY_Y;
	case Key::Z:				return GLFW_KEY_Z;
	case Key::ALPHA_0:		return GLFW_KEY_0;
	case Key::ALPHA_1:		return GLFW_KEY_1;
	case Key::ALPHA_2:		return GLFW_KEY_2;
	case Key::ALPHA_3:		return GLFW_KEY_3;
	case Key::ALPHA_4:		return GLFW_KEY_4;
	case Key::ALPHA_5:		return GLFW_KEY_5;
	case Key::ALPHA_6:		return GLFW_KEY_6;
	case Key::ALPHA_7:		return GLFW_KEY_7;
	case Key::ALPHA_8:		return GLFW_KEY_8;
	case Key::ALPHA_9:		return GLFW_KEY_9;
	case Key::LEFT_SHIFT:		return GLFW_KEY_LEFT_SHIFT;
	case Key::LEFT_CONTROL:	return GLFW_KEY_LEFT_CONTROL;
	case Key::LEFT_ALT:		return GLFW_KEY_LEFT_ALT;
	case Key::RIGHT_SHIFT:	return GLFW_KEY_RIGHT_SHIFT;
	case Key::RIGHT_CONTROL:	return GLFW_KEY_RIGHT_CONTROL;
	case Key::RIGHT_ALT:		return GLFW_KEY_RIGHT_ALT;
	case Key::SPACE:			return GLFW_KEY_SPACE;
	case Key::COMMA:			return GLFW_KEY_COMMA;
	case Key::MINUS:			return GLFW_KEY_MINUS;
	case Key::PERIOD:			return GLFW_KEY_PERIOD;
	case Key::SLASH:			return GLFW_KEY_SLASH;
	case Key::SEMICOLON:		return GLFW_KEY_SEMICOLON;
	case Key::EQUAL:			return GLFW_KEY_EQUAL;
	case Key::ESCAPE:			return GLFW_KEY_ESCAPE;
	case Key::ENTER:			return GLFW_KEY_ENTER;
	case Key::TAB:			return GLFW_KEY_TAB;
	case Key::BACKSPACE:		return GLFW_KEY_BACKSPACE;
	case Key::RIGHT:			return GLFW_KEY_RIGHT;
	case Key::LEFT:			return GLFW_KEY_LEFT;
	case Key::UP:				return GLFW_KEY_UP;
	case Key::DOWN:			return GLFW_KEY_DOWN;
	case Key::KP_0:			return GLFW_KEY_KP_0;
	case Key::KP_1:			return GLFW_KEY_KP_1;
	case Key::KP_2:			return GLFW_KEY_KP_2;
	case Key::KP_3:			return GLFW_KEY_KP_3;
	case Key::KP_4:			return GLFW_KEY_KP_4;
	case Key::KP_5:			return GLFW_KEY_KP_5;
	case Key::KP_6:			return GLFW_KEY_KP_6;
	case Key::KP_7:			return GLFW_KEY_KP_7;
	case Key::KP_8:			return GLFW_KEY_KP_8;
	case Key::KP_9:			return GLFW_KEY_KP_9;
	case Key::KP_DIVIDE:		return GLFW_KEY_KP_DIVIDE;
	case Key::KP_MULTIPLY:	return GLFW_KEY_KP_MULTIPLY;
	case Key::KP_ADD:			return GLFW_KEY_KP_ADD;
	case Key::KP_SUBTRACT:	return GLFW_KEY_KP_SUBTRACT;
	}

	return 0;
}



Keyboard& Keyboard::getInstance() {

	static Keyboard instance;

	return instance;
}

void Keyboard::update() {

	GLFWwindow* window = Window::getInstance().getWindow();

	this->currentTime = std::chrono::steady_clock::now();

	for (int i = 0; i < this->currentState.size(); i++) {

		Key key = Key(i);

		this->previousState[i] = this->currentState[i];
		this->currentState[i] = glfwGetKey(window, getKeyValue(key)) == GLFW_PRESS;

		bool current = this->currentState[i];
		bool previous = this->previousState[i];

		if (previous == false && current == true) {
			this->updatePressTime(key);
		}

		if (previous == true && current == false) {
			this->updateReleaseTime(key);
		}
	}
}

bool Keyboard::held(Key key) const {

	return this->currentState[(int)key];
}
bool Keyboard::pressed(Key key) const {

	return this->currentState[(int)key] && !this->previousState[(int)key];
}
bool Keyboard::released(Key key) const {

	return !this->currentState[(int)key] && this->previousState[(int)key];
}

double Keyboard::heldFor(Key key) const {

	if (this->held(key)) {
		return std::chrono::duration_cast<std::chrono::nanoseconds>(this->currentTime - this->pressTimes[(uint32_t)key].current).count() / 1000000.0;
	}

	return 0;
}
double Keyboard::releasedFor(Key key) const {

	if (!this->held(key)) {
		return std::chrono::duration_cast<std::chrono::nanoseconds>(this->currentTime - this->releaseTimes[(uint32_t)key].current).count() / 1000000.0;
	}

	return 0;
}

double Keyboard::pressDelta(Key key) const {

	return std::chrono::duration_cast<std::chrono::nanoseconds>(this->pressTimes[(uint32_t)key].current - this->pressTimes[(uint32_t)key].previous).count() / 1000000.0;
}
double Keyboard::releaseDelta(Key key) const {

	return std::chrono::duration_cast<std::chrono::nanoseconds>(this->releaseTimes[(uint32_t)key].current - this->releaseTimes[(uint32_t)key].previous).count() / 1000000.0;
}

	

void Keyboard::updatePressTime(Key key) {

	this->pressTimes[(uint32_t)key].previous = this->pressTimes[(uint32_t)key].current;
	this->pressTimes[(uint32_t)key].current = this->currentTime;
}
void Keyboard::updateReleaseTime(Key key) {

	this->releaseTimes[(uint32_t)key].previous = this->releaseTimes[(uint32_t)key].current;
	this->releaseTimes[(uint32_t)key].current = this->currentTime;
}
