#pragma once

#include "Includes.hpp"



enum class AlignmentVertical : uint32_t {
	TOP,
	MIDDLE,
	BOTTOM
};

enum class AlignmentHorizontal : uint32_t {
	LEFT,
	MIDDLE,
	RIGHT
};
