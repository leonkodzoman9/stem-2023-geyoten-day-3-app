#include "ImageUtility.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"



namespace ImageUtility {

    ImageRGBA8 load(std::string path) {

        ImageRGBA8 image;

        int components;
        uint8_t* imageData = stbi_load(path.data(), &image.size.x, &image.size.y, &components, 4);

        if (imageData == nullptr) {
            return image;
        }

        image.data.resize((int64_t)image.size.x * image.size.y);
        std::memcpy(image.data.data(), imageData, (int64_t)image.size.x * image.size.y * sizeof(glm::u8vec4));

        stbi_image_free(imageData);

        return image;
    }
    void savePNG(std::string path, const ImageRGBA8& image) {
        stbi_write_png(path.data(), image.size.x, image.size.y, 4, image.data.data(), 0);
    }
    void saveJPG(std::string path, const ImageRGBA8& image, int quality) {
        stbi_write_jpg(path.data(), image.size.x, image.size.y, 4, image.data.data(), quality);
    }
    void saveBMP(std::string path, const ImageRGBA8& image) {
        stbi_write_bmp(path.data(), image.size.x, image.size.y, 4, image.data.data());
    }
	
    Image<uint8_t, 1> generateSDF(const Image<uint8_t, 1>& image, float spread) {

        Image<float, 2> inner(image.size);
        Image<float, 2> outer(image.size);
        for (int i = 0; i < image.size.y; i++) {
            for (int j = 0; j < image.size.x; j++) {
                inner.at(i, j) = image.at(i, j).x < 128 ? glm::vec2(1e9) : glm::vec2(0);
                outer.at(i, j) = image.at(i, j).x < 128 ? glm::vec2(0) : glm::vec2(1e9);
            }
        }

        const auto&& Compare = [](Image<float, 2>& image, glm::vec2& point, int x, int y, int offsetx, int offsety) {

            int nx = x + offsetx;
            int ny = y + offsety;
            if (!(nx >= 0 && nx < image.size.x && ny >= 0 && ny < image.size.y)) { return; }

            glm::vec2 other = image.at(ny, nx);
            other.x += offsetx;
            other.y += offsety;

            if (glm::dot(other, other) < glm::dot(point, point))
                point = other;
        };

        for (int y = 0; y < inner.size.y; y++)
        {
            for (int x = 0; x < inner.size.x; x++)
            {
                glm::vec2 p = inner.at(y, x);
                Compare(inner, p, x, y, -1, 0);
                Compare(inner, p, x, y, 0, -1);
                Compare(inner, p, x, y, -1, -1);
                Compare(inner, p, x, y, 1, -1);
                inner.at(y, x) = p;
            }

            for (int x = inner.size.x - 1; x >= 0; x--)
            {
                glm::vec2 p = inner.at(y, x);
                Compare(inner, p, x, y, 1, 0);
                inner.at(y, x) = p;
            }
        }
        for (int y = inner.size.y - 1; y >= 0; y--)
        {
            for (int x = inner.size.x - 1; x >= 0; x--)
            {
                glm::vec2 p = inner.at(y, x);
                Compare(inner, p, x, y, 1, 0);
                Compare(inner, p, x, y, 0, 1);
                Compare(inner, p, x, y, -1, 1);
                Compare(inner, p, x, y, 1, 1);
                inner.at(y, x) = p;
            }

            for (int x = 0; x < inner.size.x; x++)
            {
                glm::vec2 p = inner.at(y, x);
                Compare(inner, p, x, y, -1, 0);
                inner.at(y, x) = p;
            }
        }

        for (int y = 0; y < outer.size.y; y++)
        {
            for (int x = 0; x < outer.size.x; x++)
            {
                glm::vec2 p = outer.at(y, x);
                Compare(outer, p, x, y, -1, 0);
                Compare(outer, p, x, y, 0, -1);
                Compare(outer, p, x, y, -1, -1);
                Compare(outer, p, x, y, 1, -1);
                outer.at(y, x) = p;
            }

            for (int x = outer.size.x - 1; x >= 0; x--)
            {
                glm::vec2 p = outer.at(y, x);
                Compare(outer, p, x, y, 1, 0);
                outer.at(y, x) = p;
            }
        }
        for (int y = outer.size.y - 1; y >= 0; y--)
        {
            for (int x = outer.size.x - 1; x >= 0; x--)
            {
                glm::vec2 p = outer.at(y, x);
                Compare(outer, p, x, y, 1, 0);
                Compare(outer, p, x, y, 0, 1);
                Compare(outer, p, x, y, -1, 1);
                Compare(outer, p, x, y, 1, 1);
                outer.at(y, x) = p;
            }

            for (int x = 0; x < outer.size.x; x++)
            {
                glm::vec2 p = outer.at(y, x);
                Compare(outer, p, x, y, -1, 0);
                outer.at(y, x) = p;
            }
        }

        Image<float, 1> combined(image.size);
        for (int i = 0; i < inner.size.y; i++) {
            for (int j = 0; j < inner.size.x; j++) {
                combined.at(i, j).x = std::sqrt(glm::dot(outer.at(i, j), outer.at(i, j))) - std::sqrt(glm::dot(inner.at(i, j), inner.at(i, j)));
            }
        }

        Image<uint8_t, 1> sdf(image.size);
        for (int i = 0; i < combined.data.size(); i++) {
            float distance = std::clamp((combined.data[i].x + spread) / (spread * 2), 0.0f, 1.0f);
            sdf.data[i].x = distance * 255;
        }

        return sdf;
    }
}

