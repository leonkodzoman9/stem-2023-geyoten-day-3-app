#include "BufferDescriptorGL.hpp"

#include "BufferGL.hpp"



BufferDescriptorGL::BufferDescriptorGL() {
    glCreateVertexArrays(1, &this->ID);
}
BufferDescriptorGL::~BufferDescriptorGL() {
    glDeleteVertexArrays(1, &this->ID);
}

BufferDescriptorGL& BufferDescriptorGL::operator = (BufferDescriptorGL&& descriptor) noexcept {

    if (this != &descriptor) {
        std::swap(this->ID, descriptor.ID);
    }

    return *this;
}

const BufferDescriptorID BufferDescriptorGL::getID() const {
    return this->ID;
}

void BufferDescriptorGL::use() const {
    glBindVertexArray(this->ID);
}

void BufferDescriptorGL::setLayout(std::initializer_list<BDSegmentLayout> segments, const BufferGL& indexBuffer) {

    int index = 0;
    for (const BDSegmentLayout& segment : segments) {

        glVertexArrayVertexBuffer(this->ID, index, segment.buffer->getID(), 0, segment.byteStride);
        glVertexArrayAttribFormat(this->ID, index, (uint32_t)segment.component, GL_FLOAT, GL_FALSE, segment.byteOffset);
        glVertexArrayAttribBinding(this->ID, index, index);
        glEnableVertexArrayAttrib(this->ID, index);

        index++;
    }

    glVertexArrayElementBuffer(this->ID, indexBuffer.getID());
}


