#include "Utility.hpp"



namespace Utility {

	std::u32string UTF8ToUTF32(std::string UTF8Chars) {

		std::u32string UTF32Chars;

		for (int i = 0; i < UTF8Chars.size();) {

			if ((UTF8Chars[i] & 0x80) == 0x00) {
				UTF32Chars.push_back(UTF8Chars[i] & 0x7F);
				i += 1;
			}
			else if ((UTF8Chars[i] & 0xE0) == 0xC0) {
				UTF32Chars.push_back(((UTF8Chars[i + 0] & 0x1F) << 6) | ((UTF8Chars[i + 1] & 0x3F) << 0));
				i += 2;
			}
			else if ((UTF8Chars[i] & 0xF0) == 0xE0) {
				UTF32Chars.push_back(((UTF8Chars[i + 0] & 0x0F) << 12) | ((UTF8Chars[i + 1] & 0x3F) << 6) | ((UTF8Chars[i + 2] & 0x3F) << 0));
				i += 3;
			}
			else if ((UTF8Chars[i] & 0xF8) == 0xF0) {
				UTF32Chars.push_back(((UTF8Chars[i + 0] & 0x07) << 18) | ((UTF8Chars[i + 1] & 0x3F) << 12) | ((UTF8Chars[i + 2] & 0x3F) << 6) | ((UTF8Chars[i + 3] & 0x3F) << 0));
				i += 4;
			}
			else {
				break;
			}
		}

		return UTF32Chars;
	}
}