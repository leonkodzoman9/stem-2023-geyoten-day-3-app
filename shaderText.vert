#version 460 core

uniform mat4 orthographic;



struct Character {
    vec4 positions[4];
    vec4 uvs[4];
};

layout (std430, binding = 0) buffer Text {
    Character characters[];           
} text;



uniform vec2 texSize;



out vec2 fragmentUV;
out flat int layer;



void main() {

    vec3 position = text.characters[gl_InstanceID].positions[gl_VertexID].xyz;

    gl_Position = orthographic * vec4(position, 1);
    
    fragmentUV = text.characters[gl_InstanceID].uvs[gl_VertexID].xy / texSize;
    layer = int(text.characters[gl_InstanceID].uvs[gl_VertexID].w);
}  